package parcial2.pucmm.parcial2.entidades;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Formulario implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long codigo;

    @Column(columnDefinition = "LONGTEXT")
    private String nombre;

    @Column(columnDefinition = "LONGTEXT")
    private String sector;

    private Double latitud;

    private Double longitud;

    @Enumerated(EnumType.ORDINAL)
    private NivelEscolar nivelEscolar;

    public NivelEscolar getNivelEscolar() {
        return nivelEscolar;
    }

    public void setNivelEscolar(NivelEscolar nivelEscolar) {
        this.nivelEscolar = nivelEscolar;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }
}
